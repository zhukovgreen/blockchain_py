import pathlib

from setuptools import setup, find_packages


packages = find_packages(exclude=['tests'])
base_dir = pathlib.Path(__file__).parent


with open(base_dir / 'README.md', encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='blockchain_py',
    use_scm_version=True,
    install_requires=['attrs'],
    long_description='\n' + long_description,
    packages=packages,
    setup_requires=['setuptools_scm'],
)
