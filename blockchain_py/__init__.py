import hashlib
import json
from time import time
from typing import Optional
from urllib.parse import urlparse

import attr
import requests


@attr.s
class Blockchain:
    chain: list = attr.ib(default=attr.Factory(list))
    current_transactions: list = attr.ib(default=attr.Factory(list))
    nodes: set = attr.ib(default=attr.Factory(set))

    def __attrs_post_init__(self):
        self.new_block(previous_hash=1, proof=100)

    def register_node(self, address: str) -> None:
        """Add a new node to the list of nodes."""
        parsed_url = urlparse(address)
        self.nodes.add(parsed_url.netloc)

    def valid_chain(self, chain: list) -> bool:
        """Determine if a given blockchain is valid.

        :param chain: A blockchain
        :return: True if valid, False if not
        """
        last_block = chain[0]
        current_index = 1

        while current_index < len(chain):
            block = chain[current_index]
            print(f'{last_block}')
            print(f'{block}')
            print("\n-----------\n")

            # Check that the hash of the block is correct
            if block['previous_hash'] != self.hash(last_block):
                return False

            # Check that the Proof of Work is correct
            if not self.valid_proof(last_block['proof'], block['proof']):
                return False

            last_block = block
            current_index += 1

        return True

    def resolve_conflicts(self):
        """
        This is our Consensus Algorithm, it resolves conflicts
        by replacing our chain with the longest one in the network.
        :return: <bool> True if our chain was replaced, False if not
        """

        neighbours = self.nodes
        new_chain = None

        # We're only looking for chains longer than ours
        max_length = len(self.chain)

        # Grab and verify the chains from all the nodes in our network
        for node in neighbours:
            response = requests.get(f'http://{node}/chain')

            if response.status_code == 200:
                length = response.json()['length']
                chain = response.json()['chain']

                # Check if the length is longer and the chain is valid
                if length > max_length and self.valid_chain(chain):
                    max_length = length
                    new_chain = chain

        # Replace our chain if we discovered a new, valid chain longer than ours
        if new_chain:
            self.chain = new_chain
            return True

        return False


    def new_block(self, proof: int, previous_hash: Optional[str] = None) -> dict:
        """Create a new Block in the Blockchain.

        :param proof: The proof given by the proof of Work Algorithm
        :param previous_hash: Hash of previous block
        :return: New Block
        """
        block = {
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'transaction': self.current_transactions,
            'proof': proof,
            'previous_hash': previous_hash or self.hash(self.chain[-1]),
        }

        # Reset the current list of transactions
        self.current_transactions = []

        self.chain.append(block)
        return block

    def new_transaction(self, sender: str, recipient: str, amount: float) -> int:
        """Create a new transaction to go into the next mined block.

        :param sender: Address of the Sender
        :param recipient: Address of the Recipient
        :param amount: Amount
        :return: The index of the block that will hold this transaction
        """
        self.current_transactions.append({
            'sender': sender,
            'recipient': recipient,
            'amount': amount,
        })
        return self.last_block['index'] + 1

    def proof_of_work(self, last_proof: int):
        """Simple PoW algorithm.

        Finds a number p' such that hash(pp') contains leading 4 zeros,
        where p is the previous p'.
        p is the previous proof, and p' is the new proof

        :param last_proof: last proof
        :return: the value of a proof
        """
        proof = 0
        while not self.valid_proof(last_proof, proof):
            proof += 1
        return proof


    def valid_proof(self, last_proof: int, proof: int):
        """ Validate the proof: does hash(last_proof, proof) contains 0000.

        :param last_proof: previous proof
        :param proof: current proof
        :return: True if correct, False if not
        """
        guess = f'{last_proof}{proof}'.encode()
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash[:4] == '0000'

    @staticmethod
    def hash(block: dict) -> str:
        """Create SHA-256 hash of a  Block.

        :param block: Block
        :return: hash
        """
        block_string = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(block_string).hexdigest()


    @property
    def last_block(self):
        return self.chain[-1]
